<div class="footer" style="text-align:left;background-image: url('<?php echo base_url('assets/'); ?>images/footer_new.jpg');">
  <div class="container">
    <div class="row">
      <div class="col-lg-3">
        <p class="mb-4"><img src="<?php echo base_url('assets/'); ?>images/LOGO.png" alt="Image" class="img-fluid" style="background:white;"></p>

      </div>
      <div class="col-lg-3">
        <h3 class="footer-heading" style="font-weight:504px;">Our Services</h3>
        <ul class="list-unstyled">
            <li style="color:white;">Fabrication</li>
            <li style="color:white;">Erection</li>
            <li style="color:white;">Labour Supply</li>
            <li style="color:white;">Plant Maintainance</li>
              <li style="color:white;">Turnkey Projects</li>

        </ul>
      </div>
      <div class="col-lg-3">
          <h3 class="footer-heading">Quick links</h3>
          <ul class="list-unstyled">
              <li style="text-align:left;"><a href="#">Home</a></li>
              <li><a href="#about_us">About Us</a></li>
              <li><a href="#services">Services</a></li>
              <li><a href="#why_chose_us">Why Choose Us</a></li>
              <li><a href="#our_work">Our Work</a></li>

              <li><a href="#contact_us">Contact Us</a></li>
          </ul>
      </div>
      <div class="col-lg-3">
          <h3 class="footer-heading">Contact</h3>
          <ul class="list-unstyled">
              <li><a href="#"><i class="fa fa-envelope fa-2" aria-hidden="true" style="color:white;"></i>greatway.pvt@yahoo.com</a></li>
              <li><a href="#"><i class="fa fa-phone fa-4" aria-hidden="true" style="color:white;"></i>9764667670</a></li>
              <li><a href="#"><p><i class="fa fa-map-marker fa-4" aria-hidden="true" style="color:white;"></i>Flat No3, Ground Floor
              B Wing, Shivam Apt,Near World Wine
              Opp.Madhur Hotel, Navapur Road,
            Oswal Empire, Boisar(W)-401501, Maharashtra</p></a></li>

          </ul>
      </div>
    </div>

    <div class="row">
      <div class="col-12">
        <div class="copyright">
            <p>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All Rights Reserved | Developed by <a href="https://teradoengineering.com/" target="_blank"  by <a href="https://teradoengineering.com" target="_blank" >Terado Engineering</a>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
              </p>
        </div>
      </div>
      <i class="fa fa-arrow-circle-up" onclick="topFunction()" id="myBtn" title="Go to top" aria-hidden="true"></i>




    </div>
  </div>
</div>


</div>
<!-- .site-wrap -->


<!-- loader -->
<div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#ff5e15"/></svg></div>

<script src="<?php echo base_url('assets/'); ?>js/jquery-3.3.1.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/jquery-migrate-3.0.1.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/jquery-ui.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/popper.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/owl.carousel.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/jquery.stellar.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/jquery.countdown.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/jquery.easing.1.3.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/aos.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/jquery.fancybox.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/jquery.sticky.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/jquery.mb.YTPlayer.min.js"></script>




<script src="<?php echo base_url('assets/'); ?>js/main.js"></script>
<script>
//Get the button
var mybutton = document.getElementById("myBtn");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
</script>

</body>

</html>
