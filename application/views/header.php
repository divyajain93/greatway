<!DOCTYPE html>
<html lang="en">

<head>

  <title>Greatway</title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" href="<?php echo base_url('assets/images/logo_small.png') ?>" type="image/x-icon">
  <link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700|Muli:300,400" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url('assets/');?>fonts/icomoon/style.css">

  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/jquery-ui.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/owl.carousel.min.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/owl.theme.default.min.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/owl.theme.default.min.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/jquery.fancybox.min.css">

  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/bootstrap-datepicker.css">

  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>fonts/flaticon-2/font/flaticon.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>fonts/flaticon-2/font/_flaticon.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/aos.css">
  <link href="<?php echo base_url('assets/'); ?>css/jquery.mb.YTPlayer.min.css" media="all" rel="stylesheet" type="text/css">
 <link rel="stylesheet" type="text/css" href="http://localhost/Greatway/css_root/flaticon.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/style.css">
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />

  <style>
  .list_style{
    color:#2C39AA;font-weight:599;
  }
  .hr_border{
    border-top:2px solid black;
  }
  .arrow_color{
    color:black;
  }

  .banner_text{
    position: absolute;
top: 130px;
background: rgb(0, 0, 0);
background:
#2C39AA;
color:white;
width: 42%;
padding: -8px;
margin-left: 16%;
height: 233px;
text-align: center;
font-size: 20px;
padding-top: 2%;
padding-left: 2%;
padding-right: 2%;
opacity: 89%
  }


  @media screen and (max-width : 1204px)
  {
  .banner_text
    {  position: absolute;top:110px;
background: rgb(0, 0, 0);
background:#2C39AA;color:white;width: 59%;padding: -8px;
margin-left: 8%;height: 215px;text-align: center;
font-size: 17px;padding-top: 2%;padding-left: 4%;
padding-right: 4%;
opacity: 89%;
    }
  }


  @media screen and (min-width : 1204px)
  {
  .banner_text
    {  position: absolute;
      top: 150px;
      background: rgb(0, 0, 0);
      background:#2C39AA;color:white;width: 55%;padding: -8px;margin-left: 8%;height: 233px;text-align: center;
  font-size: 22px;padding-top: 2%;padding-left: 4%;padding-right: 4%;opacity: 89%;
    }
  }


  @media screen and (max-width : 414px)
  {
  .banner_text
    {position: absolute;top: 13px;background: rgb(0, 0, 0);background:#2C39AA;color:white;
      width: 67%;padding: -8px;margin-left: 6%;height: 100px;text-align: center;
font-size: 7px;padding-top: 1%;padding-left: 0%;padding-right: 0%;opacity: 89%;
    }

  }

  @media only screen and (max-width: 768px) and (min-width: 415px)
  {
  .banner_text
    {position: absolute;top: 21px;
background: rgb(0, 0, 0);
background:#2C39AA;color:white;
width: 60%;padding: -8px;margin-left: 12%;height: 139px;text-align: center;
font-size: 10px;padding-top: 1%;padding-left: 1%;padding-right: 1%;opacity: 89%;
    }
  }




  .border_key{
    border-style: solid;
    border-color: black;
  }

  .key_text{
    display: inline;
    font-size: 13px;
    margin-top: 5%;
    padding: 0%;
    float: right;
    font-weight: 500;
  }

  .key_image{
  display: inline;
  margin:5%;margin-right:3%;width: 12%;
  }


  @media screen and (min-width : 1204px)
  {
    .key_text
    {
      font-size:20px;
    }
  }

    @media only screen and (max-width:576px) and (min-width:386px)
  {
    .key_text
    {
      font-size:14px;
    }
  }

  @media only screen and (max-width: 768px) and (min-width:576px)
      {
        .key_text{
          display: inline;
          font-size: 9px;
          margin-top: 5%;
          padding: 0%;
          float: right;
          font-weight: 500;
        }
      }

      @media only screen and (max-width:366px)
          {
            .key_text{
              display: inline;
              font-size: 11px;
              margin-top: 5%;
              padding: 0%;
              float: right;
              font-weight: 500;
            }
          }



          @media only screen and (max-width: 1203px) and (min-width:992px)
              {
                .key_text{
                  display: inline;
                  font-size: 18px;
                  margin-top: 5%;
                  padding: 0%;
                  float: right;
                  font-weight: 500;
                }
              }

  .list_style{
    color:#2C39AA;font-weight:599;
  }
  .hr_border{
    border-top:2px solid black;
  }
  .arrow_color{
    color:black;
  }

  .img_border{
    width:75%;
    height: 40%;
     float: left;
     right:25%;border: 3px solid black;">
  }


  .img_text{
    display:inline;font-size:30px;
  }

  @font-face {
  font-family: Farro-regular;
  src: url(<?php echo base_url('assets/'); ?>farro/Farro-regular.otf);
}

#myBtn {
  display: none;
  position: fixed;
  bottom: 20px;
  right: 30px;
  z-index: 99;
  font-size: 35px;
  border: none;
  outline: none;
  color: white;
  cursor: pointer;
  padding: 15px;
  border-radius: 4px;
}

#myBtn:hover {
  background-color: #555;
}
  </style>

</head>

<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">

  <div class="site-wrap">

    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <i class="fa fa-tasks fa-5 icon-close2 js-menu-toggle" aria-hidden="true"></i>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>



    <div class="header-top bg-light">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-6 col-lg-3">
            <a href="index.html">
              <img src="<?php echo base_url('assets/'); ?>images/LOGO.png" alt="Image" class="img-fluid">
            </a>
          </div>
          <div class="col-lg-3 d-none d-lg-block">

            <div class="quick-contact-icons d-flex">

            </div>

          </div>
          <div class="col-lg-3 d-none d-lg-block">
            <div class="quick-contact-icons d-flex">

            </div>
          </div>

          <div class="col-lg-3 d-none d-lg-block">
            <div class="quick-contact-icons d-flex">
              <div class="icon align-self-start">

              </div>
              <div class="text">
                  <p class="caption-text"><i class="fa fa-mobile fa-6" aria-hidden="true"></i>Call Us On:</p>
                <p class="h4 d-block"> + 91 9764667670</p>

              </div>
            </div>
          </div>

          <div class="col-6 d-block d-lg-none text-right">
              <a href="#" class="d-inline-block d-lg-none site-menu-toggle js-menu-toggle text-black"><span
                class="icon-menu h3"></span></a>
          </div>
        </div>
      </div>

      <div class="site-navbar py-2 js-sticky-header site-navbar-target d-none pl-0 d-lg-block" role="banner">

      <div class="container">
        <div class="d-flex align-items-center">

          <div class="mr-auto" style="text-align:center;">
            <nav class="site-navigation position-relative text-right" role="navigation">
              <ul class="site-menu main-menu js-clone-nav mr-auto d-none pl-0 d-lg-block">



                <li style="background:#2c39AA;">
                  <a href="#" class="nav-link text-left"><img src="<?php echo base_url('assets/'); ?>/images/s_logo.png" style="width: 60%;"></a>
                </li>
                <li class="active">
                  <a href="#" class="nav-link text-left">Home</a>
                </li>
                <li>
                  <a href="#about_us" class="nav-link text-left">About Us</a>
                </li>
                <li>
                  <a href="#services" class="nav-link text-left">Services</a>
                </li>

                <li><a href="#why_chose_us" class="nav-link text-left">Why Choose Us</a></li>

                <li>
                    <a href="#contact_us" class="nav-link text-left">Contact Us</a>
                  </li>
              </ul>                                                                                                                                                                                                                                                                                          </ul>
            </nav>

          </div>

        </div>
      </div>
    </div>
    </div>
