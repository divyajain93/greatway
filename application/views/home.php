<div>
<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" id="about_us">

        <img src="<?php echo base_url('assets/'); ?>images/Banner-01.jpg" alt="Image" class="img-fluid" style="width:100%;">
        <div class="banner_text">
           <h3 style="color:white;"><strong><u>About Greatway</u></strong></h3>
           <p style="color:white;">Greatway Fabricators and Erectors is one of the most respected
             Engineering and Project management service provider
             to diverse industries in South Gujarat and west Maharashtra.</p>
         </div>

  </div>
</div>
  <hr id="services">
<br>
<br>

    <!-- END slider -->

<!--projects-->
<div>
  <div class= "container">
  <h3 class="headings"><u>Our Services</u></h3><br>

      <div class="row">

 <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 py-3" style="position: relative;
  text-align: center;
  color: white;">
   <img src="<?php echo base_url('assets/'); ?>images/S1.jpg" style="width:85%;height:75%;border:1px solid black;">
    <div class="centered"><strong>ERECTION</strong></div>
 </div>
 <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 py-3" style="position: relative;
  text-align: center;
  color: white;">

   <img src="<?php echo base_url('assets/'); ?>images/S2.jpg" alt="Forest" style="width:85%;height:75%;border:1px solid black;">
  <div class="centered"><strong>FABRICATION</strong></div>
 </div>
</div>
<div class="row">
 <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 py-3" style="position: relative;
  text-align: center;
  color: white;">
   <img src="<?php echo base_url('assets/'); ?>images/S3.jpg" alt="Mountains" style="width:85%;height:75%;border:1px solid black;">
  <div class="centered"><strong>LABOUR SUPPLY</strong></div>
 </div>

 <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 py-3" style="position: relative;
  text-align: center;
  color: white;">
   <img src="<?php echo base_url('assets/'); ?>images/S4.jpg" alt="Mountains" style="width:85%;height:75%;border:1px solid black;">
  <div class="centered"><strong>PLANT MAINTAINANCE</strong></div>
 </div>
</div>
<div class="row">
 <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 py-3" style="position: relative;
  text-align: center;
  color: white;">
   <img src="<?php echo base_url('assets/'); ?>images/S5.jpg" alt="Mountains" style="width:85%;height:75%;border:1px solid black;">
  <div class="centered"><strong>TURNKEY PROJECTS</strong></div>
 </div>
</div>
</div>
<div class="Clearfix">
</div>
</div>
</div>

<!--end
projects-->

<!--key facts-->


<hr id="why_chose_us">
<br>
<br>

<section>
  <div class="container">
    <h3 class="headings"> Key Facts</h3>
    <div class="row key_header" >
      <div class ="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 py-3" >
        <div class="border_key" style="height:100%;">

          <div>
             <img src="<?php echo base_url('assets/'); ?>images/01.png" class="key_image">
<p class="key_text">Proven History of about 300 projects&nbsp;
  <br>delivered till date.</p>
          </div>
        </div>
      </div>

      <div class ="col-xl-6 col-lg-6 col-md-6 col-sm-6 py-3" >
        <div class="border_key" style="height:100%;">
          <div style="display:inline;">
            <img src="<?php echo base_url('assets/'); ?>images/02.png" class="key_image">
            <p class="key_text">Team of 150 to 200 team of skilled&nbsp;&nbsp;&nbsp;&nbsp;
              <br>labour and expanding further.</p>
        </div>
      </div>
</div>

      <div class ="col-xl-6 col-lg-6 col-md-6 col-sm-6 py-3">
        <div class="border_key" style="height:100%;">
          <div style="display:inline;">
            <img src="<?php echo base_url('assets/'); ?>images/03.png" class="key_image">
            <p class="key_text">Labour supply contracts with 7 FTSE&nbsp;
              <br>companies.</p>
  </div>
      </div>
    </div>
    <div class ="col-xl-6 col-lg-6 col-md-6 col-sm-6 py-3">
      <div class="border_key" style="height:100%;">
        <div style="display:inline;">
          <img src="<?php echo base_url('assets/'); ?>images/05.png" class="key_image">
          <p class="key_text">Clients list spanning upto 7 diverse&nbsp;&nbsp;&nbsp;
            <br>companies.</p>
</div>
    </div>
  </div>
  </div>
</div>
</section>
<hr id="our_work">
<br><br>
<!--end keyfacts-->
<!--our work start-->
<section>
  <div>
    <h3 class="headings">Our Work</h3>

  <div class="hero-slide owl-carousel site-blocks-cover">

    <div class="intro-section" style="background-image: url('<?php echo base_url('assets/'); ?>images/slider_background.jpg');">

      <div class="container">

        <div class="row align-items-center">
          <div class="col-lg-6 mx-auto text-center" data-aos="fade-up">
            <span class="d-block"></span>

            <img src="<?php echo base_url('assets/'); ?>images/P1.jpg" class="img_border">

             <p style="display:inline;font-size:32px;">Gujarat Gas Limited</p>


          </div>
        </div>
      </div>
    </div>
    <div class="intro-section" style="background-image: url('<?php echo base_url('assets/'); ?>images/slider_background.jpg');">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-6 mx-auto text-center" data-aos="fade-up">
            <span class="d-block"></span>
            <img src="<?php echo base_url('assets/'); ?>images/P2.jpg" class="img_border">

<p style="display:inline;font-size:32px;">Tarapur Environmental Protection Society</p>
          </div>
        </div>
      </div>
    </div>


    <div class="intro-section" style="background-image: url('<?php echo base_url('assets/'); ?>images/slider_background.jpg');">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-6 mx-auto text-center" data-aos="fade-up">
            <span class="d-block"></span>
            <img src="<?php echo base_url('assets/'); ?>images/P4.jpg" class="img_border">
<p style="display:inline;font-size:32px;">Incinator - Coromandel International Limited</p>
          </div>
        </div>
      </div>
    </div>

    <div class="intro-section" style="background-image: url('<?php echo base_url('assets/'); ?>images/slider_background.jpg');">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-6 mx-auto text-center" data-aos="fade-up">
            <span class="d-block"></span>
            <img src="<?php echo base_url('assets/'); ?>images/P5.jpg" class="img_border">
<p style="display:inline;font-size:32px;">Chimney Installation</p>
          </div>
        </div>
      </div>
    </div>

    <div class="intro-section" style="background-image: url('<?php echo base_url('assets/'); ?>images/slider_background.jpg');">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-6 mx-auto text-center" data-aos="fade-up">
            <span class="d-block"></span>
              <img src="<?php echo base_url('assets/'); ?>images/P6.jpg" class="img_border">
<p style="display:inline;font-size:32px;">Sulphuric - Atul Limited</p>
          </div>
        </div>
      </div>
    </div>
    <div class="intro-section" style="background-image: url('<?php echo base_url('assets/'); ?>images/slider_background.jpg');">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-6 mx-auto text-center" data-aos="fade-up">
            <span class="d-block"></span>
            <img src="<?php echo base_url('assets/'); ?>images/P7.jpg" class="img_border">
<p style="display:inline;font-size:32px;">Lupin Pithampur Plant</p>
          </div>
        </div>
      </div>
    </div>
    <div class="intro-section" style="background-image: url('<?php echo base_url('assets/'); ?>images/slider_background.jpg');">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-6 mx-auto text-center" data-aos="fade-up">
            <span class="d-block"></span>
            <img src="<?php echo base_url('assets/'); ?>images/P8.jpg" class="img_border" >
<p style="display:inline;font-size:32px;">Production and Warehouse Building Construction</p>
          </div>
        </div>
      </div>
    </div>
    <div class="intro-section" style="background-image: url('<?php echo base_url('assets/'); ?>images/slider_background.jpg');">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-6 mx-auto text-center" data-aos="fade-up">
            <span class="d-block"></span>
            <img src="<?php echo base_url('assets/'); ?>images/P3.jpg" class="img_border">
<p style="display:inline;font-size:32px;margin-top:10%;">Milton Towers - Mumbai</p>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
</section>
<!--our work ends-->

<!--our client start-->
<br>
<br>
<section>

  <div class ="container" id="our_clients">
<h3 class="headings"><u>Our Clients</u></h3><br>
<div class="row">
  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12" style="margin: 0%;padding:0%";>
    <ul style="list-style-type: none;">
      <li class ="list_style"><i class="fa fa-angle-right arrow_color" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Reliance,Hajra Surat</li>
      <hr class="hr_border">
        <li class ="list_style"><i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Gujarat Gas limited

  <hr class="hr_border">
          <li class ="list_style"><i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;GTBL-Gujarat Themis Biosyn Limited</li>  <hr class="hr_border">
            <li class ="list_style"><i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Atul Limited,Gujarat</li>  <hr class="hr_border">
              <li class ="list_style"><i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Milton Tower Mumbai</li>  <hr class="hr_border">
                <li class ="list_style"><i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Richter Themis Mesicre Limited</li>  <hr class="hr_border">
</ul>
</div>
<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12" style="margin: 0%;padding:0%";>
            <ul style="list-style-type: none;">
                <li class ="list_style"><i class<i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Mecleods Pharmaceuticals Limited</li>  <hr class="hr_border">

                    <li class ="list_style"><i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Atul Bioscience Limited</li>  <hr class="hr_border">
                      <li class ="list_style"><i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;WAM India Private Limited</li>  <hr class="hr_border">
                        <li class ="list_style"><i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Coromondel International Limited(SOGL)</li>  <hr class="hr_border">
  <li class ="list_style"><i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;TEPS-Tarapur Environmental Protection Society</li>  <hr class="hr_border">
  </ul>
</div>
<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12" style="margin: 0%;padding:0%";>
                        <ul style="list-style-type: none;">
                          <li class ="list_style"><i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;CEPT-Common Effluent Treatment Plant</li>  <hr class="hr_border">
                            <li class ="list_style"><i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Amal Limited</li>  <hr class="hr_border">
                              <li class ="list_style"><i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;BKT-Balkrishna Tyres,Dombivali</li>  <hr class="hr_border">
                                <li class ="list_style"><i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Mazagoan Dock Shipbuilders Limited</li>  <hr class="hr_border">
                                  <li class ="list_style"><i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Arav Fragnances & Flavours Pvt Ltd</li>  <hr class="hr_border">

</ul>
</div>
</div>
<div class="site-section block-3">
  <div class="container">
    <div class="projects-carousel-wrap">
      <div class="owl-carousel owl-slide-3">
        <div class="project-item">
          <img src="<?php echo base_url('assets/'); ?>images/clients/logo_1.png">
        </div>
        <div class="project-item">
          <img src="<?php echo base_url('assets/'); ?>images/clients/logo_2.png">
        </div>
        <div class="project-item">
          <img src="<?php echo base_url('assets/'); ?>images/clients/logo_3.png">
        </div>
        <div class="project-item">
          <img src="<?php echo base_url('assets/'); ?>images/clients/logo_4.png">
        </div>
        <div class="project-item">
          <img src="<?php echo base_url('assets/'); ?>images/clients/logo_5.png">
        </div>
        <div class="project-item">
          <img src="<?php echo base_url('assets/'); ?>images/clients/logo_6.png">
        </div>
        <div class="project-item">
          <img src="<?php echo base_url('assets/'); ?>images/clients/logo_7.png">
        </div>
        <div class="project-item">
          <img src="<?php echo base_url('assets/'); ?>images/clients/logo_8.png">
        </div>
        <div class="project-item">
          <img src="<?php echo base_url('assets/'); ?>images/clients/logo_9.png">
        </div>
        <div class="project-item">
          <img src="<?php echo base_url('assets/'); ?>images/clients/logo_10.png">
        </div>
        <div class="project-item">
          <img src="<?php echo base_url('assets/'); ?>images/clients/logo_11.png">
        </div>
        <div class="project-item">
          <img src="<?php echo base_url('assets/'); ?>images/clients/logo_12.png">
        </div>
        <div class="project-item">
          <img src="<?php echo base_url('assets/'); ?>images/clients/logo_13.png">
        </div>
        <div class="project-item">
          <img src="<?php echo base_url('assets/'); ?>images/clients/logo_14.png">
        </div>
        <div class="project-item">
          <img src="<?php echo base_url('assets/'); ?>images/clients/logo_15.png">
        </div>
        <div class="project-item">
          <img src="<?php echo base_url('assets/'); ?>images/clients/logo_16.png">
        </div>
        <div class="project-item">
          <img src="<?php echo base_url('assets/'); ?>images/clients/logo_17.png">
        </div>
        <div class="project-item">
            <img src="<?php echo base_url('assets/'); ?>images/clients/logo_18.png">
          </div>
        <div class="project-item">
          <img src="<?php echo base_url('assets/'); ?>images/clients/logo_19.png">
        </div>
        <div class="project-item">
          <img src="<?php echo base_url('assets/'); ?>images/clients/logo_21.png">
        </div>

        </div>
      </div>
    </div>

  </div>
</div>

</section>
<hr id="contact_us">
<br>
<br>
<!--our client ends-->

    <!-- END block-2 -->
<!---contact us --->
<section>
  <div class="container">

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h3 class="headings">Contact Us</h3><br>
               <form action="<?php echo base_url('Mail/send'); ?>" method="POST"  class="myform">
                   <div class="row">
                       <div class="col-lg-4">

                         <ul class="list-unstyled"
                             <li><a href="#"><p style="color:#2C39AA;font-weight:20;"><i class="fa fa-envelope fa-3" aria-hidden="true"></i>greatway.pvt@yahoo.com</p></a></li>
                             <li><a href="#"><p style="color:#2C39AA;font-weight:20;"><i class="fa fa-phone fa-4" aria-hidden="true"></i>9764667670</p></a></li>
                             <li><a href="#"><p style="color:#2C39AA;font-weight:20;"><i class="fa fa-map-marker fa-4" aria-hidden="true"></i>Flat No.3, Ground Floor
                             B Wing, Shivam Apt, Near World Wine
                             Opp.Madhur Hotel, Navapur Road,
                            Oswal Empire, Boisar(W)-401501, Maharashtra</p></a></li>

                         </ul>
                       </div>
                       <div class="col-lg-4">
                           <div class="form-group">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Full Name*" required>
                          </div>
                          <div class="form-group">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Enter email address" required>
                          </div>
                          <div class="form-group">
                            <input type="text" class="form-control" id="subject" name="subject" placeholder="Enter your Subject*" required>
                          </div>

                       </div>
                       <div class="col-lg-4">
                           <div class="form-group">
                                 <textarea class="form-control" rows="5" id="message"  name="message" placeholder='Message' required></textarea>
                            </div>
 <input type="submit" value="Send Message" class="btn btn-primary btn-lg" style="background-color:#2C39AA;float:right;">
                       </div>
                   </div>
                   <div class="row">
                       <div class="col-lg-4 col-md-4 col-sm-4">

                       </div>
                   </div>
                </form>
            </div>
          </div>
          </section>
          <br>
            <!--ends contact us-->
